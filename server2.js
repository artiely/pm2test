const Koa = require('koa')
var Router = require('koa-router')
var app = new Koa()
var router = new Router()
router.get('/', (ctx, next) => {
   return (ctx.body = '你好')
})
app.use(router.routes()).use(router.allowedMethods())
app.listen(3000, () => {
  console.log('启动成功')
})
